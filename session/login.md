# **Login V2**

Use this service to login in application. This service also return the session id.

## **Endpoint**

### Production Enviroment

```
    POST    http://apialfarero.eliabc.com:80/v1.0/login
```

### Develpment Enviromment

```
    POST    http://159.203.93.24:8080/v1.0/login
```

## **Request Parameters**

### Query Parameters
| QUERY PARAMETER | VALUE | DESCRIPTION | RULE |
|---|---|---|:---:|
| firstLogin | `1` Indicates if the user is logging in for the first time on their device. `0` indicates that not is first login. | Indicates first login | *Required* |

### Headers Fields

| HEADER FIELD | VALUE | DESCRIPTION | RULE |
|---|---|---|:---:|
| Content-Type | `application/json; multipart/form-data;` | URL encoded form | *Required* |
| Authorization | `Basic c951985e404309115...` | A valid access token from the eliab API | *Required* |
| OS | `Android`, `iOS`| Device OS version from where the request is made | *Optional* |
| AppVersion | The version must have the `Semantic Versioning 2.0.0` format. Values: `X.Y.Z`. Example: `1.0.0, 2.0.0, etc`  | Version of the application installed on the device. | *Optional* |
| DeviceUUID | `ADJKSLJSF-F12GKHJGK-DFQFASDAF` | Device Universally Unique Identifier | *Optional* |
| OsVersion |  `Android-11.0`, `iOS-13` | Version of the OS installed on the device. | *Optional* |
| DeviceModel | `Samsung Galaxy S20` | Device model. | *Optional* |
| Timezone | `UTC-6` | Time zone from where the request is made. | *Optional* |
| Language | `ES`, `ENG`, `ITA`| Language of the request source device | *Optional* |
| PlayerID | `ADJKSLJSF-F12GKHJGK-DFQFASDAF`  | **`Onesignal`** device subscription id. | *Optional* |

### Raw Body Content

You must send within the `Request Content` object a `Login Request` object in JSON format.

## **Response Format**

On success, the HTTP status code in the response header is `200` OK and the response body contains an `Info` and `Response Content` objects in JSON format. Within `Response Content` object return a `Login Response` JSON  object. On error, the header status code is an `error code` and the response body contains an `Info` object whit error information.

---

## **Login Request Object (Full)**

| KEY	 | VALUE | TYPE | RULES |
|-----|-------------|------|-------|
| user | Email of owner | String | *Required* |
| password | Current password | String | *Required* |

## **Login Response Object (Full)**

| KEY	 | VALUE | TYPE | RULES |
|-----|-------------|------|-------|
| id | User id | Int64 | *Required* |
| fist_name | User's fist name | String | *Required* |
| last_name | User's last name | String | *Required* |
| email | User's email | String | *Required* |
| phone | User's phone number | String | *Required* |
| address | User's residence address | String | *Required* |
| schools | Schools where the user's children are enrolled | Array(Object) | *Required* |
| schools[].id | School id | Int64 | *Required* |
| schools[].name | School name | String | *Required* |
| schools[].logo | School logo URL. | String | *Required* |
| schools[].description | Descriptive information about the school | String | *Optional* |


## **Example**

### Request 

```php
curl --location --request POST '159.203.93.24:8080/v1.0/login?firstLogin=1' \
--header 'Content-Type: application/json; multipart/form-data;' \
--header 'Sessionid: ' \
--header 'AppVersion: 1.0.10' \
--header 'OsVersion:  Android-11.4' \
--header 'OS:  Android' \
--header 'DeviceModel:  Samsung Galaxy S20' \
--header 'Timezone:  UTC-6' \
--header 'Language:  Es' \
--header 'PlayerID:  SHH54G2HGG32HJ3J-2GL34K5...' \
--header 'Authorization: Basic dGVzdDp0ZXN0' \
--data-raw '{
    "requestContent": {
        "user": "josefernandez@eliabc.com",
        "password": "example"
    }
}'

```

### Response

``` json
{
    "info": {
        "type": "success",
        "title": "Successful!",
        "message": "The request has been successful!",
        "sessionId": "9b2c8bc91941dc89795cd0268d30f50a3c436531b6e99ea34d3e638a9c1c4eca8490ef75ce4..."
    },
    "responseContent": {
        "id": 6,
        "first_name": "José",
        "last_name": "Fernandez",
        "email": "josefernandez@eliabc.com",
        "phone": "12341234",
        "address": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "schools": [
            {
                "id": 1,
                "name": "Mi Casa",
                "logo": "https://www.nasa.gov/sites/default/files/thumbnails/image/stsci-h-p2042a-f-1663x1663.png",
                "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            },
            {
                "id": 2,
                "name": "El Castillo",
                "logo": "https://www.nasa.gov/sites/default/files/thumbnails/image/stsci-h-p2042a-f-1663x1663.png",
                "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            }
        ]
    }
}
```

< [Home](../home.md)
